#!/bin/bash

read -r n1
read -r n2
read -r n3
read -r n4

mkdir "./$n1"
echo "# $n1" > "./$n1/README.md"
date +"Data de hoje: %Y-%m-%d" >> "./$n1/README.md"

mkdir "./$n2"
echo "# ./$n2" > "$n2/README.md"
date +"Data de hoje: %Y-%m-%d" >> "./$n2/README.md"

mkdir "./$n3"
echo "# ./$n3" > "./$n3/README.md"
date +"Data de hoje: %Y-%m-%d" >> "./$n3/README.md"

mkdir "./$n4"
echo "# ./$n4" > "./$n4/README.md"
date +"Data de hoje: %Y-%m-%d" >> "./$n4/README.md"
