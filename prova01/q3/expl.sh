#!/bin/bash

v1="Isso é uma variável"
nu=42

echo "Valor de minha_variavel: $v1"
echo "Valor de numero: $nu"

echo "Diferença entre solicitar ao usuário e receber parâmetros de linha de comando:"
echo "1. Solicitar ao usuário para digitar o valor de uma variável:"
echo "Por favor, digite um valor para v2:"
read v2
echo "Você digitou: $v2"

echo "2. Receber variáveis como parâmetros de linha de comando:"
echo "Você pode executar este script com argumentos, por exemplo:"
echo "./seuscript.sh valor1 valor2"
echo "Argumento 1: \$1 = $1"
echo "Argumento 2: \$2 = $2"

echo "Variáveis automáticas em Bash:"
echo "Nome do script: \$0 = $0"
echo "Número de argumentos: \$# = $#"
echo "Último argumento: \${!#} = ${!#}"
echo "Código de retorno do último comando: \$? = $?"
