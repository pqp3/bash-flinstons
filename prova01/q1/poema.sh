#!/bin/bash

echo "por um instante tudo está bem."
sleep 1
echo "E em um piscar de olhos,"
sleep 1
echo "do hospital você é refém."
sleep 1
echo "A vida é um sopro,"
sleep 1
echo "Então viva com intensidade."
sleep 1
echo "Não queira mais escutar,"
sleep 1
echo "barulho de choro, E sim risos de felicidade."
sleep 1
echo "Claro que todos nós,passamos dificuldade. Mas precisamos de força pra encarar, essa dura realidade."
sleep 1
echo "Tudo tão corrido e você,não tem tempo,"
sleep 1
echo "pra se destrair"
sleep 1
