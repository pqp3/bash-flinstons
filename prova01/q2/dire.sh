#!/bin/bash

echo "Digite o nome do primeiro diretório:"
read d1

echo "Digite o nome do segundo diretório:"
read d2

echo "Digite o nome do terceiro diretório:"
read d3

echo "Diretório: $d1"
echo "Quantidade de arquivos com a extensão .xls: $(ls $d1 | grep -cE '\.xls$')"
echo "Quantidade de arquivos com a extensão .bmp: $(ls $d1 | grep -cE '\.bmp$')"
echo "Quantidade de arquivos com a extensão .docx: $(ls $d1 | grep -cE '\.docx$')"

echo "Diretório: $d2"
echo "Quantidade de arquivos com a extensão .xls: $(ls $d2 | grep -cE '\.xls$')"
echo "Quantidade de arquivos com a extensão .bmp: $(ls $d2 | grep -cE '\.bmp$')"
echo "Quantidade de arquivos com a extensão .docx: $(ls $d2 | grep -cE '\.docx$')"

echo "Diretório: $d3"
echo "Quantidade de arquivos com a extensão .xls: $(ls $d3 | grep -cE '\.xls$')"
echo "Quantidade de arquivos com a extensão .bmp: $(ls $d3 | grep -cE '\.bmp$')"
echo "Quantidade de arquivos com a extensão .docx: $(ls $d3 | grep -cE '\.docx$')"

