#!/bin/bash

set_color() {
}

set_two_lines() {
    PS1="\u\w\n\$ "
}

set_only_username() {
    PS1="\u $ "
}

reset_prompt() {
    PS1='\u@\h:\w\$ '
}

