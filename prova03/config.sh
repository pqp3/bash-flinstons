#!/bin/bash

source func.sh

choose_customization() {
    echo "Escolha a customização:"
    echo "1. Mudar cor"
    echo "2. Exibir apenas o nome do usuário"
    echo "3. Prompt em duas linhas"

    read -p "Opção: " choice

    case "$choice" in
        1)
            read -p "Informe o código da cor (ex: 0;34 para azul): " color_code
            ;;
        2)
            set_only_username
            ;;
        3)
            set_two_lines
            ;;
        *)
            echo "Opção inválida."
            ;;
    esac
}


choose_customization

bash export PS1=$PS1
