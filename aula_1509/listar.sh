#!/bin/bash

echo -e "\$USER - Retorna o Nome do Usuário: " $USER

echo -e "\$UID - Retorna o ID do Usuário: " $UID

echo -e "\$PWD - Retorna o Diretório Atual: " $PWD

echo -e "\$HOME - Retorna o Diretório HOME do Usuário Atual: " $HOME

echo -e "\$PATH - Obtém o PATH Atual do Sistema: " $PATH

echo -e "\$LINES - Retorna o Número de Linhas do Terminal: " $LINES

echo -e "\$COLUMNS - Retorna o Número de Colunas do Terminal: " $COLUMNS

echo -e "\$BASH_VERSION - Armazena a versão do shell bash em execução: " $BASH_VERSION

echo -e "\$CDPATH - Caminho de busca do comando cd: " $CDPATH

echo -e "\$HOSTNAME - Nome do computador: " $HOSTNAME

echo -e "\$HOSTTYPE - Para obter a arquitetura da maquina: " $HOSTTYPE

echo -e "\$GROUPS - Retorna informações sobre o GID: " $GROUPS

echo -e "\$PPID - Obtém o ID do processo pai: " $PPID

echo -e "\$RANDOM - Retorna um número aleatório entre 0 e 32767: " $RANDOM

echo -e "\$SECONDS - Para obter a quantos segundos o script está rodando: " $SECONDS

echo -e "\$0 - Obtém o nome do script: " $0

echo -e "\$$ - PID atual: " $$

echo -e "\$! - ID de uma tarefa em background: " $!

echo -e "\$SHELL - Configuração de caminho do shell que interpretará os comandos digitados: " $SHELL

echo -e "\$OSTYPE - Detecta o Sistema Operacional: " $OSTYPE
