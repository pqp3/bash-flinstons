#!/bin/bash


function lt(){
	unzip -l "$1"
}
function pr(){
	unzip -p "$1" "$2"
}
function ad(){
	zip -j -u "$1" "$2"
}
function ra(){
	zip -d "$1" "$2"
}
function ex(){
	unzip "$1"
}
function es(){
	unzip "$1" "$2"
}


echo "Ferramenta para manipular arquivos .zip"
echo 
read -p "Digite 1 se o arquivo existe e 2 caso queira cria-lo:" wr
echo

case $wr in

	1) 
		read -p "Nome do .zip que deseja manipular: " arq_zip 
		if  [ "${arq_zip##*.}" != "zip" ] || [ ! -e "$arq_zip" ]; then
			echo
			echo "ERRO: arquivo inexistente ou não '.zip'"
			exit 1
		fi
		echo 
		;;
		
	2) 
		ip=()
		while true; do
       			read -p "Digite o nome do arquivo que deseja manipular e 'x' para continuar: " arq
			echo
			
			if [ "$arq" == "x" ]; then
			       break
		       	else
				if [ ! -e "$arq" ]; then
			 		echo "ERRO: arquivo ou caminho inexistente."
					echo
				else
					ip+=("$arq")
				fi
			fi			
		done 

		echo
		read -p "Nome do arquivo de destino: " arq_zip
		echo

		arq_zip="$arq_zip.zip"

		zip -j "$arq_zip" "${ip[0]}" > /dev/null
		
		for i in "${ip[@]:1}"; do
			zip -j -u "$arq_zip" "$i" > /dev/null
		done

		echo
		echo "Arquivo $arq_zip criado com sucesso."
		echo
		;;

	*) 	
		echo "Opção inválida." 
		exit 1
		;;

esac

while true; do
	
	echo
	echo "================================================================"
	echo "1) Listar o conteúdo do arquivo .zip"
	echo "2) Pré-visualizar algum arquivo que está compactado neste .zip."
	echo "3) Adicionar arquivos ao .zip."
	echo "4) Remover arquivos ao .zip."
	echo "5) Extrair todo o conteúdo do .zip."
	echo "6) Extrair arquivo especifico do .zip."
	echo 
	echo "X) Sair do programa."
	echo "================================================================"
	echo
	read -p "Opção:" op
	echo
	echo

	case $op in
	
		1) 
			lt $arq_zip ;;
		2) 
			read -p "Digite o nome do arquivo que deseja visualizar dentro do zip: " arq
			echo "Conteúdo do arquivo:"
			pr $arq_zip $arq
			;;
		3) 	read -p "Digite o caminho do arquivo que deseja adicionar ao zip: " arq 
			ad $arq_zip $arq
			;;
		4) 	read -p "Digite o caminho do arquivo que deseja remover do zip: " arq 
			ra $arq_zip $arq	
			;;
		5) 	ex $arq_zip
			;;
		6) 	read -p "Digite o caminho do arquivo que deseja extrair do zip: " arq 
			es $arq_zip $arq
			;;
		X) exit 1;;
		*) echo "ERRO: Opção inválida!" ;;
	esac
done


