#!/bin/bash

v=$1
d="/home/cassio"
u=$(echo $(whoami))

if [ $u != "cassio" ]; then
	echo "Esse programa só pode ser executado pelo usuário cassio"
	exit
fi

if [ -z "$v" ]; then
	echo $(find $d -mindepth 1 -maxdepth 1 -type f | wc -l)
	exit
else
	echo -e "Quantidade de arquivos: $(find $v -mindepth 1 -maxdepth 1 -type f | wc -l)\nQuantidade de diretórios: $(find $v -mindepth 1 -maxdepth 1 -type d | wc -l)"
fi
